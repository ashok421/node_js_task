steps to run the file in project:

1.All the files are in master branches there are total 3 files and each file for each task and each file should be run individually

2.file named task1.js is for function to check odd and even number and this file is run by command 
"node file1.js" in terminal

3.file named fibonacci_series.js is for generating fibonacci series and this file is run by command 
"node fibonacci_series.js  with one input argument number " in terminal

4.file named task3.js is for function to filter out the odd and even numbers from given array and this file is run by command  "node file3.js" in terminal
